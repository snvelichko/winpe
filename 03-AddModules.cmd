@ECHO OFF
SET ADKPATH=C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit
REM Adding PowerShell
Dism /image:PE\mount /Add-Package /PackagePath:"%ADKPATH%\Windows Preinstallation Environment\x86\WinPE_OCs\WinPE-NetFx.cab"
Dism /image:PE\mount /Add-Package /PackagePath:"%ADKPATH%\Windows Preinstallation Environment\x86\WinPE_OCs\WinPE-PowerShell.cab"
Dism /image:PE\mount /Add-Package /PackagePath:"%ADKPATH%\Windows Preinstallation Environment\x86\WinPE_OCs\WinPE-DismCmdlets.cab"
Dism /image:PE\mount /Add-Package /PackagePath:"%ADKPATH%\Windows Preinstallation Environment\x86\WinPE_OCs\WinPE-WMI.cab"
Dism /image:PE\mount /Add-Package /PackagePath:"%ADKPATH%\Windows Preinstallation Environment\x86\WinPE_OCs\WinPE-StorageWMI.cab"
PAUSE