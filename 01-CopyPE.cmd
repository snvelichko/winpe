@ECHO OFF
SET CURRENTPATH=%~dp0
SET ADKPATH=C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit
call "%ADKPATH%\Deployment Tools\DandISetEnv.bat"
call "%ADKPATH%\Windows Preinstallation Environment\copype.cmd" x86 "%CURRENTPATH%PE"
cd /d "%CURRENTPATH%PE\media"
for /D %%i in (*-*) do rmdir /S /Q "%%i"
cd /d "%CURRENTPATH%"
copy PE\media\sources\boot.wim PE\boot.wim
PAUSE