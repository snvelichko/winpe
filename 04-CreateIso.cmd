@ECHO OFF
SET CURRENTPATH=%~dp0
SET ADKPATH=C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit
call "%ADKPATH%\Deployment Tools\DandISetEnv.bat"
cd /d %CURRENTPATH%
DEL /F /Q "PE\media\sources\boot.wim"
DISM /Export-Image /SourceImageFile:PE\boot.wim /SourceIndex:1 /DestinationImageFile:PE\media\sources\boot.wim /Compress:max
mkdir PE\iso
oscdimg -bootdata:2#p0,e,b"PE\fwfiles\etfsboot.com"#pEF,e,b"PE\fwfiles\efisys.bin" -u1 -udfver102 PE\media PE\iso\winpe.iso
PAUSE