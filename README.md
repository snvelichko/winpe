ОПИСАНИЕ
---------------

Скрипты и инструменты для сборки Windows Preinstall Environment (WinPE) 5.1

ИНСТРУМЕНТЫ
---------------

Необходимые инструменты для сборки.

- Windows ADK (http://www.microsoft.com/ru-ru/download/details.aspx?id=39982) - Комплект средств для развертывания и оценки Windows
- bbLean (http://bb4win.sourceforge.net/bblean/) - Лёгкая оболочка рабочего стола
- PE Network Manager (http://holger.winbuilder.net/) - Утилита настройки сети для Windows PE
- VirtualBox (https://www.virtualbox.org) - Виртуальная машина, для тестирования

СКРИПТЫ
---------------

Описание скриптов.

- 01-CopyPE.cmd - Инициализация необходимых каталогов, создаёт каталог "PE" (перед запуском необходимо удалить данный каталог, в противном случае скрипт не будет копировать нужные файлы во избежания случайной потери информации)
- 02-Mount.cmd - Монтирует образ PE\winpe.wim в каталог PE\mount
- 03-AddDriver.cmd - Добавляет в образ драйвера расположенные в папке Drivers
- 03-AddLang.cmd - Добавляет в образ поддержку русского языка
- 03-AddModules.cmd - Добавляет в образ дополнительные модули, в скрипте приведён пример интаграции PowerShell
- 03-Commit.cmd - Сохраняет изменения проведённые в каталоге PE\mount в образ PE\winpe.wim
- 04-CreateIso.cmd - Копирует с сжатием образ PE\winpe.wim в PE\ISO\Sources\boot.win и создаёт iso образ в PE\winpe.iso
- 04-Unmount-Commit.cmd - Размонтирует образ из каталога PE\mount с сохранением изменений
- 04-Unmount-Discard.cmd - Размонтирует образ из каталога PE\mount без сохранения изменений

КАТАЛОГИ И ФАЙЛЫ
---------------

Описание каталогов и файлов.

- PE - Каталог с рабочими файлами
- PE\media - Каталог с файлами для сборки в iso образ
- PE\mount - Каталог для монтирования образа wim
- PE\fwfiles - Загрузчик для iso образа
- PE\iso\winpe.iso - Iso образ
- PE\winpe.wim - Wim образ
- Tools - Каталог с дополнительными инструментами
- Tools\bbLean - Настроенный и урезанный bbLean
- Tools\bbLean\plugins - Плагины
- Tools\bbLean\styles - Файлы стилей
- Tools\bbLean\Blackbox.rc - Общий файл настроек
- Tools\bbLean\Extensions.rc - Файл настроек расширений
- Tools\bbLean\Menu.rc - Главное меню
- Tools\bbLean\plugins.rc - Список подключаемых плагинов
- Tools\pagefile.cmd - Скрипт подключения файла подкачки
- Tools\winpeshl.ini - Файл параметров для запуска оболочки